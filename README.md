
# Portfolio

This is my portfolio with what I have been able to publish publicly.

Everything is sorted by programming languages and projects, most of the empty projects has been lost, i have the intentions to recreate them. ( they are empty for file organization)

---
## Author

**Luis Medina** 
* *My profile on* [LinkedIn][linkedin-url]
* *My my end-of-course project in unity vr using autohands* [Proyect][final-project]
## Showcase

This project contains (Also my knowledge in programming):

* Java
  * JAVA Base
  * JDK
  * SPRING
  * HIBERNATE
  * ANDROID STUDIO
* C#
  * UNITY
  * .NET (No project available)
* WEB DEVELOPMENT
  * HTML/CSS
  * PHP
  * LARAVEL
  * DOM
  * JQUERY
  * ANGULAR
  * NODE.JS
* PYTHON
  * PYTHON Base
  * DJANGO
  * TENSORFLOW
* SGDB
  * MYSQL/MARIADB
  * POSTGRE SQL
  * MONGODB
  * SQLITE
  * PL/SQL

<!-- Markdown link & img dfn's -->

[linkedin-url]: [www.linkedin.com/in/luis-medina-fernández-394a98151]
[final-project]: [https://gitlab.com/Grayblood/proyecto-final]