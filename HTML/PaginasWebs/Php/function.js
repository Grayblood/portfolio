$(document).ready(function () {
    $("#submit").click(function () {
        var name = $("#name").val();
        var password = $("#password").val();
        if (name.length == "" || password.length == ""){
            $("#message").html("please fill out this field first").fadeIn();
            $("#message").addClass("error");
            return false;
        }else {
            $.ajax({
                type: 'POST',
            url: 'redirect.php',
            data: { name: name, password: password },
            success: function (feedback) {
                $("#text").html(feedback);
            }
        });
        }
    });
    $(".name_error_text").hide();
    $(".password_error_text").hide();
    var error_password = false;

    $("#password").focusout(function () {
        check_password();
    });

    function check_password() {
        $("#message").hide();
        var password_length = $("#password").val().length;
        if (password_length < 8) {
            $(".password_error_text").html("Should be at least 8 characters");
            $(".password_error_text").show().addClass("error");
            error_password = true;
        } else {
            $(".password_error_text").hide();
        }
    }
});