<?php
include 'Session.php';
Session::checkSession(); //Checking Session whether user logged in or not
if (isset($_GET['action']) && $_GET['action'] == "logout") {
    Session::destroy();
    exit();
}