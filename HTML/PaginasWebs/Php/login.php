<?php

include "Session.php";
Session::checkLogin(); // Checking whether user is logged in or not.
?>
<html>
    <body>
<h2>User Login</h2>
<p id="text"></p>
<p id="message"></p>
<form action="redirect.php" method="post">
    <table>
        <tr>
            <input type="name" id="name" placeholder="Enter name" name="name">
            <span>
                <p class="email_error_text"></p>
            </span>
        </tr>
        <tr>
            <input type="password" id="password" placeholder="Enter password" name="password">
            <span>
                <p class="password_error_text"></p>
            </span>
        </tr>
        <tr>
            <input type="submit" id="submit" value="Submit" style="margin-top: 5px;">
        </tr>
    </table>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"> </script> <script src="function.js"></script>
<style type="text/css"> .error{ color:red; font-size: 20px; } </style>
<body>
</html>